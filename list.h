/**
 * \file list.h
 * \brief A Handy generic list library
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-21 Tue 06:06 PM
 * This file contains the headers of all the routines that manages \a List object type.
 *
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __LIST_H__
	#define __LIST_H__

	#include <stdio.h>
	#include <stdlib.h>
	#include <errno.h>
	#include <string.h>
	#include <stdbool.h>

	/**
	 * \def LIST
	 * \brief User-level working objects.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST(TYPE, NAME) \
		typedef struct ListElement##NAME { \
			TYPE * value; \
			struct ListElement##NAME * next; \
			struct ListElement##NAME * previous; \
		} ListElement##NAME; \
		typedef struct { \
			ListElement##NAME * head; \
			ListElement##NAME * tail; \
			size_t size; \
		} List##NAME;

	/**
	 * \def LIST_MALLOC
	 * \brief Wrapper routine for \a List_malloc routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_MALLOC(TYPE, NAME) \
		static List##NAME * List##NAME##_malloc(void) { \
			return (List##NAME *)List_malloc(); \
		}

	/**
	 * \def LIST_ISEMPTY
	 * \brief Wrapper routine for \a List_isempty routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_ISEMPTY(TYPE, NAME) \
		static bool List##NAME##_isempty(const List##NAME * const list) \
		{ \
			return List_isempty((List *)list); \
		}

	/**
	 * \def LISTELEMENT_ISHEADSENTINEL
	 * \brief Wrapper routine for \a ListElement_isheadsentinel routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LISTELEMENT_ISHEADSENTINEL(TYPE, NAME) \
		static bool ListElement##NAME##_isheadsentinel(const ListElement##NAME * const element) \
		{ \
			return ListElement_isheadsentinel((ListElement *)element); \
		}

	/**
	 * \def LISTELEMENT_ISTAILSENTINEL
	 * \brief Wrapper routine for \a ListElement_istailsentinel routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LISTELEMENT_ISTAILSENTINEL(TYPE, NAME) \
		static bool ListElement##NAME##_istailsentinel(const ListElement##NAME * const element) \
		{ \
			return ListElement_istailsentinel((ListElement *)element); \
		}

	/**
	 * \def LISTELEMENT_GET
	 * \brief Gets the value of a given element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LISTELEMENT_GET(TYPE, NAME) \
		static TYPE ListElement##NAME##_get(const ListElement##NAME * const element) \
		{ \
			return *element->value; \
		}

	/**
	 * \def LISTELEMENT_SET
	 * \brief Sets a given element to a given value.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LISTELEMENT_SET(TYPE, NAME) \
		static void ListElement##NAME##_set(ListElement##NAME * const element, const TYPE value) \
		{ \
			*element->value = value; \
		}

	/**
	 * \def LIST_AT_HEAD
	 * \brief Wrapper routine for \a List_at_head routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_AT_HEAD(TYPE, NAME) \
		static  ListElement##NAME * List##NAME##_at_head(const List##NAME * const list) \
		{ \
			return (ListElement##NAME *)List_at_head((List *)list); \
		}

	/**
	 * \def LIST_AT_TAIL
	 * \brief Wrapper routine for \a List_at_tail routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_AT_TAIL(TYPE, NAME) \
		static ListElement##NAME * List##NAME##_at_tail(const List##NAME * const list) \
		{ \
			return (ListElement##NAME *)List_at_tail((List *)list); \
		}

	/**
	 * \def LIST_SWAP
	 * \brief Wrapper routine for \a List_swap routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_SWAP(TYPE, NAME) \
		static void List##NAME##_swap(List##NAME * const list0, List##NAME * const list1) \
		{ \
			List_swap((List *)list0, (List *)list1); \
		}

	/**
	 * \def LIST_PUSH_FRONT
	 * \brief Wrapper routine for \a List_push_front routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_PUSH_FRONT(TYPE, NAME) \
		static void List##NAME##_push_front(List##NAME * const list, const TYPE value) \
		{ \
			List_push_front((List *)list, &value, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_PUSH_BACK
	 * \brief Wrapper routine for \a List_push_back routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_PUSH_BACK(TYPE, NAME) \
		static void List##NAME##_push_back(List##NAME * const list, const TYPE value) \
		{ \
			List_push_back((List *)list, &value, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_INSERT_FROM_HEAD
	 * \brief Wrapper routine for \a List_insert_from_head routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_INSERT_FROM_HEAD(TYPE, NAME) \
		static void List##NAME##_insert_from_head(List##NAME * const list, const size_t index, const TYPE value) \
		{ \
			List_insert_from_head((List *)list, index, &value, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_INSERT_FROM_TAIL
	 * \brief Wrapper routine for \a List_insert_from_tail routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_INSERT_FROM_TAIL(TYPE, NAME) \
		static void List##NAME##_insert_from_tail(List##NAME * const list, const size_t index, const TYPE value) \
		{ \
			List_insert_from_tail((List *)list, index, &value, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_POP_FRONT
	 * \brief Wrapper routine for \a List_pop_front routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_POP_FRONT(TYPE, NAME) \
		static void List##NAME##_pop_front(List##NAME * const list) \
		{ \
			List_pop_front((List *)list); \
		}

	/**
	 * \def LIST_POP_BACK
	 * \brief Wrapper routine for \a List_pop_back routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_POP_BACK(TYPE, NAME) \
		static void List##NAME##_pop_back(List##NAME * const list) \
		{ \
			List_pop_back((List *)list); \
		}

	/**
	 * \def LIST_REMOVE_FROM_HERE
	 * \brief Wrapper routine for \a List_remove_from_here routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-08-31 Thu 11:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_REMOVE_FROM_HERE(TYPE, NAME) \
		static void List##NAME##_remove_here(List##NAME * const list, ListElement##NAME * const here, const size_t index) \
		{ \
			List_remove_from_here((List *)list, (ListElement *)here, index); \
		}

	/**
	 * \def LIST_REMOVE_THIS
	 * \brief Wrapper routine for \a List_remove_this routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2021-05-11 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_REMOVE_THIS(TYPE, NAME) \
		static void List##NAME##_remove_this(List##NAME * const list, ListElement##NAME * const element) \
		{ \
			List_remove_this((List *)list, (ListElement *)element); \
		}

	/**
	 * \def LIST_REMOVE_FROM_HEAD
	 * \brief Wrapper routine for \a List_remove_from_head routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_REMOVE_FROM_HEAD(TYPE, NAME) \
		static void List##NAME##_remove_head(List##NAME * const list, const size_t index) \
		{ \
			List_remove_from_head((List *)list, index); \
		}

	/**
	 * \def LIST_REMOVE_FROM_TAIL
	 * \brief Wrapper routine for \a List_remove_from_tail routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_REMOVE_FROM_TAIL(TYPE, NAME) \
		static void List##NAME##_remove_tail(List##NAME * const list, const size_t index) \
		{ \
			List_remove_from_tail((List *)list, index); \
		}

	/**
	 * \def LIST_WHEREIS
	 * \brief Wrapper routine for \a List_whereis routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_WHEREIS(TYPE, NAME) \
		static ListElement##NAME * List##NAME##_whereis(const List##NAME * const list, const TYPE value) \
		{ \
			return (ListElement##NAME *)List_whereis((List *)list, &value, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_COUNT
	 * \brief Wrapper routine for \a List_count routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_COUNT(TYPE, NAME) \
		static size_t List##NAME##_count(const List##NAME * const list, const TYPE value) \
		{ \
			return List_count((List *)list, &value, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_INDEXOF
	 * \brief Wrapper routine for \a List_indexof routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_INDEXOF(TYPE, NAME) \
		static size_t List##NAME##_indexof(const List##NAME * const list, const TYPE value) \
		{ \
			return List_indexof((List *)list, &value, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_CAT
	 * \brief Wrapper routine for \a List_cat routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_CAT(TYPE, NAME) \
		static List##NAME * List##NAME##_cat(const List##NAME * const list0, const List##NAME * const list1) \
		{ \
			return (List##NAME *)List_cat((List *)list0, (List *)list1, sizeof(TYPE)); \
		}

	/**
	 * \def LIST_COPY
	 * \brief Wrapper routine for \a List_copy routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_COPY(TYPE, NAME) \
		static List##NAME * List##NAME##_copy(const List##NAME * const list) \
		{ \
			return (List##NAME *)List_copy((List *)list, sizeof(TYPE)); \
		}
	
	/**
	 * \def LIST_EMPTY
	 * \brief Wrapper routine for \a List_empty routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_EMPTY(TYPE, NAME) \
		static void List##NAME##_empty(List##NAME * const list) \
		{ \
			List_empty((List * )list); \
		}

	/**
	 * \def LIST_BIND
	 * \brief Wrapper routine for \a List_bind routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_BIND(TYPE, NAME) \
		static void List##NAME##_bind(const List##NAME * const list0, List##NAME * * const list1) \
		{ \
			List_bind((List *)list0, (List * *)list1); \
		}

	/**
	 * \def LIST_FREE
	 * \brief Wrapper routine for \a List_free routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define LIST_FREE(TYPE, NAME) \
		static void List##NAME##_free(List##NAME * * const list) \
		{ \
			List_free((List * *)list); \
		}

	/**
	 * \struct ListElement
	 * \brief Represents a List element object type.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 11:55 PM
	 * \var void * value
	 * Address of the generic data.
	 * \var QueueElement * next
	 * Address of the next list element.
	 * \var QueueElement * previous
	 * Address of the previous list element.
	 */
	typedef struct ListElement {
		void * value;
		struct ListElement * next;
		struct ListElement * previous;
	} ListElement;

	/**
	* \struct List
	* \brief Represents a List object type.
	* \author Sangsoic <sangsoic@protonmail.com>
	* \version 0.1
	* \date 2020-03-25 Wed 11:56 PM 
	* \var ListElement * head
	* Address of the head sentinel.
	* \var ListElement * tail
	* Address of the tail sentinel.
	* \var size_t size
	* Number of element contained within the list.
	*/
	typedef struct {
		ListElement * head;
		ListElement * tail;
		size_t size;
	} List;

	/**
	 * \fn static ListElement * ListElement_mallocinit(const void * const value, const size_t offset)
	 * \brief Allocates and initializes a ListElement object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 12:49 AM
	 * \param value A given value.
	 * \param offset Size in byte of the given value.
	 * \return Newly allocated and initialized ListElement object.
	 */
	static ListElement * ListElement_mallocinit(const void * const value, const size_t offset);

	/**
	 * \fn static void ListSentinel_allocinit(List * const list)
	 * \brief Allocates and initializes a sentinel element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:10 AM
	 * \param list A given list.
	 */
	static void ListSentinel_allocinit(List * const list);

	/**
	 * \fn List * List_malloc(void)
	 * \brief Allocates a list object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:21 AM
	 * \return A new allocated list.
	 */
	List * List_malloc(void);

	/**
	 * \fn bool List_isempty(const List * const list)
	 * \brief Tests if a given list is empty.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:23 AM
	 * \param list A given list.
	 * \return true if empty else false.
	 */
	bool List_isempty(const List * const list);

	/**
	 * \fn bool ListElement_isheadsentinel(const ListElement * const element)
	 * \brief Tests if a given element is the head sentinel.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 05:01 PM
	 * \param element A given element.
	 * \return true if a given element is a head sentinel else false.
	 */
	bool ListElement_isheadsentinel(const ListElement * const element);

	/**
	 * \fn bool ListElement_istailsentinel(const ListElement * const element)
	 * \brief Tests if a given element is the tail sentinel.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 05:01 PM
	 * \param element A given element.
	 * \return true if a given element is a tail sentinel else false.
	 */
	bool ListElement_istailsentinel(const ListElement * const element);

	/**
	 * \fn ListElement * List_at_head(const List * const list)
	 * \brief Returns the first element of the list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:24 AM
	 * \param list A given list.
	 * \return The first element of the list.
	 */
	ListElement * List_at_head(const List * const list);

	/**
	 * \fn ListElement * List_at_tail(const List * const list)
	 * \brief Returns the last element of the list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:24 AM
	 * \param list A given list.
	 * \return The last element of the list.
	 */
	ListElement * List_at_tail(const List * const list);

	/**
	 * \fn void List_swap(List * const list0, List * const list1)
	 * \brief Swaps contents of two given lists.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:26 AM
	 * \param list0 A given list.
	 * \param list1 A given list.
	 */
	void List_swap(List * const list0, List * const list1);


	/**
	 * \fn void List_push_front(List * const list, const void * const value, const size_t offset)
	 * \brief Adds an element at the head of a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 08:01 PM
	 * \param list A given list.
	 * \param value A given value.
	 * \param offset Number of bytes taken by the given value in memory.
	 */
	void List_push_front(List * const list, const void * const value, const size_t offset);

	/**
	 * \fn void List_push_back(List * const list, const void * const value, const size_t offset)
	 * \brief Adds an element at the tail of a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 08:01 PM
	 * \param list A given list.
	 * \param value A given value.
	 * \param offset Number of bytes taken by the given value in memory.
	 */
	void List_push_back(List * const list, const void * const value, const size_t offset);

	/**
	 * \fn ListElement * List_insert_from_head(List * const list, const void * const value, const size_t index, const size_t offset)
	 * \brief Inserts an element at a given index in a given list, starting from the head of the list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 11:15 PM
	 * \param list A given list.
	 * \param value A given value.
	 * \param index A given index (relative to the head of the list).
	 * \param offset Number of bytes taken by the given value in memory.
	 * \return The newly constructed element.
	 */
	ListElement * List_insert_from_head(List * const list, const void * const value, const size_t index, const size_t offset);

	/**
	 * \fn ListElement * List_insert_from_tail(List * const list, const void * const value, const size_t index, const size_t offset)
	 * \brief Inserts an element at a given index in a given list, starting from the tail of the list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 11:15 PM
	 * \param list A given list.
	 * \param value A given value.
	 * \param index A given index (relative to the tail of the list).
	 * \param offset Number of bytes taken by the given value in memory.
	 * \return The newly constructed element.
	 */
	ListElement * List_insert_from_tail(List * const list, const void * const value, const size_t index, const size_t offset);

	/**
	 * \fn void List_pop_back(List * const list)
	 * \brief Removes the last element of a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 08:04 PM
	 * \param list A given list.
	 */
	void List_pop_back(List * const list);

	/**
	 * \fn void List_pop_front(List * const list)
	 * \brief Removes the first element of a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 08:04 PM
	 * \param list A given list.
	 */
	void List_pop_front(List * const list);

	/**
	 * \fn void List_remove_from_here(List * const list, ListElement * const here, const size_t index)
	 * \brief Removes the element at a given index (relative to the given \a here element) of a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-08-31 Mon 02:55 AM
	 * \param list A given list.
	 * \param here Element from which to remove the element of the given \a index.
	 * \param index A given index (relative to the given \a here element of the list).
	 */
	void List_remove_from_here(List * const list, ListElement * const here, const size_t index);

	/**
	 * \fn void List_remove_from_head(List * const list, const size_t index)
	 * \brief Removes the element at a given index in a given list, starting from the head of the list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 11:14 PM
	 * \param list A given list.
	 * \param index A given index (relative to the head of the list).
	 */
	void List_remove_from_head(List * const list, const size_t index);

	/**
	 * \fn void List_remove_this(List * const list, ListElement * const element)
	 * \brief Removes the given list element from a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2021-05-11 Sun 11:14 PM
	 * \param list A given list.
	 * \param element The given element to remove.
	 */
	void List_remove_this(List * const list, ListElement * const element);

	/**
	 * \fn void List_remove_from_tail(List * const list, const size_t index)
	 * \brief Removes the element at a given index in a given list, starting from the tail of the list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 11:14 PM
	 * \param list A given list.
	 * \param index A given index (relative to the tail of the list).
	 */
	void List_remove_from_tail(List * const list, const size_t index);

	/**
	 * \fn ListElement * List_whereis(const List * const list, const void * const value, const size_t offset)
	 * \brief Returns the first element containing the given value.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:59 AM
	 * \param list A given list.
	 * \param value A given value.
	 * \param offset Size in byte of the given value.
	 * \return The first element containing the given value.
	 */
	ListElement * List_whereis(const List * const list, const void * const value, const size_t offset);

	/**
	 * \fn ListElement * List_count(const List * const list, const void * const value, const size_t offset)
	 * \brief Counts the number of elements containing a given value in a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:59 AM
	 * \param list A given list.
	 * \param value A given value.
	 * \param offset Size in byte of the given value.
	 * \return The number of elements containing a given value in a given list.
	 */
	size_t List_count(const List * const list, const void * const value, const size_t offset);

	/**
	 * \fn ListElement * List_indexof(const List * const list, const void * const value, const size_t offset)
	 * \brief Returns the index (relative to the head of the list) of the element containing a given value.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 01:59 AM
	 * \param list A given list.
	 * \param value A given value.
	 * \param offset Size in byte of the given value.
	 * \return The index (relative to the head of the list) of the element containing a given value.
	 */
	size_t List_indexof(const List * const list, const void * const value, const size_t offset);

	/**
	 * \fn List * List_cat(const List * const list0, const List * const list1, const size_t offset)
	 * \brief Concatenates two given lists.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 02:44 PM
	 * \param list0 A given list.
	 * \param list1 A given list.
	 * \param offset Size in byte of the given value.
	 * \return A newly allocated list formed by concatenation of two given lists.
	 */
	List * List_cat(const List * const list0, const List * const list1, const size_t offset);

	/**
	 * \fn List * List_copy(const List * const list, const size_t offset)
	 * \brief Copies a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 02:50 PM
	 * \param list A given list.
	 * \param offset Size in byte of the given value.
	 * \return Newly allocated copy of a given list.
	 */
	List * List_copy(const List * const list, const size_t offset);

	/**
	 * \fn void List_free(List * * const list)
	 * \brief Frees a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-12 Sun 08:05 PM
	 * \param list Address of the pointer containing the given list.
	 */
	void List_free(List * * const list);

	/**
	 * \fn void List_empty(List * const list)
	 * \brief Empties a given list.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-02 Sat 09:12 PM
	 * \param list A given list.
	 */
	void List_empty(List * const list);

	/**
	 * \fn void List_bind(List * const list0, List * * const list1)
	 * \brief Binds two given list together.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-04-21 Tue 02:51 PM
	 * \param list0 A given list.
	 * \param list1 The pointer containing the address of a given list.
	 * \a The first element of \a link1 is linked to the last element of \a link0. Then \a list1 is freed but not its content.
	 */
	void List_bind(List * const list0, List * * const list1);
#endif
